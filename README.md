# Prueba técnica

El objetivo de esta prueba técnica es demostrar tus habilidades técnicas en front y/o back.

Para agilizar los primeros pasos te facilitamos una estructura muy similar al stack que tenemos actualmente.

Siéntete libre de hacer las modificaciones que quieras.

## Objectivo principal

Creación de un chat. Las características que tiene de tener son los siguientes:

- Usuarios creados
- Poder hacer login
- Abrir un chat con otro usuario y tener una conversación

En el caso de no hacer una de las partes puedes hacer mocks o un servidor sencillo para ver la funcionalidad.

Te adjuntamos algunos ejemplos visuales para ayudarte en el diseño.

Mucha suerte!




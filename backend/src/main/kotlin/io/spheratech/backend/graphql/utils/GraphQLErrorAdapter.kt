package io.spheratech.backend.graphql.utils

import graphql.ExceptionWhileDataFetching
import graphql.GraphQLError
import graphql.language.SourceLocation

class GraphQLErrorAdapter(private val error: GraphQLError) : GraphQLError {

    override fun getExtensions(): Map<String, Any> {
        return error.extensions
    }

    override fun getLocations(): List<SourceLocation>  = error.locations

    override fun getErrorType() = error.errorType

    override fun getPath(): List<Any>  = error.path

    override fun toSpecification(): Map<String, Any>  = error.toSpecification()

    override fun getMessage(): String  = if (error is ExceptionWhileDataFetching) error.exception.message!! else error.message
}

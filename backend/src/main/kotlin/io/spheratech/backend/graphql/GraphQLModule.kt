package io.spheratech.backend.graphql

import io.spheratech.backend.serverless.ServerlessModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val GraphQLModule = Kodein.Module("graphql") {
    import(ServerlessModule)

    bind<GraphqlRun>() with singleton { GraphqlRun(instance(), instance()) }


}

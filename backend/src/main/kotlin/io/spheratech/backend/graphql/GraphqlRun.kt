package io.spheratech.backend.graphql

import com.expedia.graphql.SchemaGeneratorConfig
import com.expedia.graphql.TopLevelObject
import com.expedia.graphql.toSchema
import graphql.ExecutionInput

import graphql.GraphQL
import io.spheratech.backend.eventsourcing.EventSource
import io.spheratech.backend.eventsourcing.createEventSource
import io.spheratech.backend.eventsourcing.saveEventSource
import io.spheratech.backend.eventsourcing.updateEventSource
import io.spheratech.backend.graphql.context.ContextFromGraphQL
import io.spheratech.backend.graphql.utils.transformResult

import io.spheratech.backend.utils.json.toJson
import java.util.concurrent.CompletableFuture
import java.util.function.BiFunction
import kotlin.streams.toList

class GraphqlRun(queries: Set<Query>, mutations: Set<Mutation>): BiFunction<String, ContextFromGraphQL, CompletableFuture<String>>  {

    private var build: GraphQL

    init {
        val config = SchemaGeneratorConfig(listOf("io.spheratech.backend", "kotlin"))

        val queriesTransformed = queries.stream().map { TopLevelObject(it) }.toList()
        val mutationsTransformed = mutations.stream().map { TopLevelObject(it) }.toList()
        val graphQLSchema = toSchema(queriesTransformed, mutationsTransformed, config)
        build = GraphQL.newGraphQL(graphQLSchema).build()
    }
    override fun apply(query: String, context: ContextFromGraphQL): CompletableFuture<String> {
        val action = ExecutionInput.newExecutionInput()
            .query(query)
            .context(context)
            .build()

        val eventSource = createEventSource(query, context)

        return build.executeAsync(action)
            .thenApplyAsync { transformResult(it) }
            .thenApplyAsync{ toJson(it) }
            .thenApplyAsync{ updateAndSaveEventsource(it, eventSource)}
    }

    private fun updateAndSaveEventsource(result: String, eventSource: EventSource): String {
        val updateEventSource = updateEventSource(eventSource, result)
        saveEventSource(updateEventSource)
        return result
    }

}


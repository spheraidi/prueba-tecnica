package io.spheratech.backend.graphql.utils

import graphql.ExceptionWhileDataFetching
import graphql.ExecutionResult
import graphql.GraphQLError
import java.lang.reflect.InvocationTargetException

import java.util.stream.Collectors

fun transformResult(result: ExecutionResult): Map<String, Any> {
    val serverErrors = result.errors.stream()
        .filter { e -> !isClientError(e) }
        .map { ((it as ExceptionWhileDataFetching).exception as InvocationTargetException).targetException.message }
        .collect(Collectors.toList())

    val resultMap = result.toSpecification()
    resultMap["errors"] = serverErrors
    return resultMap
}

private fun isClientError(error: GraphQLError) = (error is ExceptionWhileDataFetching || error is Throwable).not()



package io.spheratech.backend

import io.spheratech.backend.graphql.GraphQLModule
import org.kodein.di.Kodein

val BackendModule = Kodein {
    import(GraphQLModule)
}

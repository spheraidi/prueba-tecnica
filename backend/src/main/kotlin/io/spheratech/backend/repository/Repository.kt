package io.spheratech.backend.repository

import arrow.core.Option

interface Repository<T> {
    fun save(entity: T)
    fun find(): List<T>
    fun find(entity: String): Option<T>
    fun delete(id: String)
}

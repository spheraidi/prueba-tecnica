package io.spheratech.backend.repository

interface Entity<T> {
    val id: String
    val entity: T
}

package io.spheratech.backend.repository

import org.kodein.di.Kodein

val RepositoryModule = Kodein.Module("repository") {
}

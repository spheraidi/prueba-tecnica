package io.spheratech.backend.repository

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

abstract class RepositoryInMemory<T> : Repository<T> {
    private val repository = ArrayList<Entity<T>>()

    override fun save(entity: T) {
        val entityForSave = transformEntity(entity)
        val maybeEntity = find(entityForSave.id)

        if(maybeEntity.isDefined()) {
            delete(entityForSave.id)
        }

        repository.add(entityForSave)
    }

    protected abstract fun transformEntity(entity: T): Entity<T>

    override fun find(): List<T> = repository.map { it.entity }

    override fun find(id: String): Option<T> {
        val mayBeEntity = repository.stream()
            .filter { entity -> id == entity.id }
            .map { it.entity }
            .findFirst()

        return if (mayBeEntity.isPresent) Some(mayBeEntity.get()) else  None
    }


    override fun delete(id: String) {
        repository.removeIf { entity -> id == entity.id }
    }
}

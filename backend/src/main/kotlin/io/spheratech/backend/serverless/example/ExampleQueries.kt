package io.spheratech.backend.serverless.example

import io.spheratech.backend.graphql.Query


class ExampleQueries: Query {

    fun hello(): String = "Hello world!"

}

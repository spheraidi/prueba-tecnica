package io.spheratech.backend.serverless.example

import io.spheratech.backend.graphql.Mutation


class ExampleMutation: Mutation {

    fun mutation(text: String): String = text

}

package io.spheratech.backend

import io.javalin.Context
import io.javalin.Javalin

import io.spheratech.backend.graphql.GraphqlRun
import io.spheratech.backend.graphql.context.ContextFromGraphQL
import org.kodein.di.direct
import org.kodein.di.generic.instance

class Server {
    private lateinit var app: Javalin

    fun start(port: Int) {
        val graphql: GraphqlRun = BackendModule.direct.instance()


        app = Javalin.create()
            .apply {
                disableStartupBanner()
                enableCorsForAllOrigins()
                enableCaseSensitiveUrls()
                port(port)
                get("/") { it.result("Hello World") }
                get("/graphql") { it.contentType("text/html; charset=UTF-8").result(getGraphQLi()) }
                post("/graphql") { ctx ->
                    val query = ctx.bodyAsClass(Map::class.java).get("query").toString()
                    val context = getContextGraphQL(ctx)
                    ctx.result(graphql.apply(query, context))
                }
            }
            .start()

    }


    fun stop() = app.stop()

    private fun getContextGraphQL(ctx: Context): ContextFromGraphQL = ContextFromGraphQL("")

    private fun getGraphQLi() = javaClass.classLoader.getResourceAsStream("index.html")

}
